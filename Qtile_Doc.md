                                    **Qtile Window Manager **
__
**Forked From https://github.com/arcolinux/arcolinux-qtile**

**Installation **

**1) Get all dependencies**

**Debian Based **

sudo apt install volumeicon-alsa  fonts-roboto  ttf-anonymous-pro rofi i3lock
xclip xsel compton qt5-style-plugins materia-gtk-theme lxappearance   nautilus ranger  xfce4-power-manager  playerctl&&  gnome-keyring  network-manager-applet  xfce4-notifyd  
mate-polkit  

**Note :** To install qtile on debian stable it is advise to build from source , on debian unstable you can install the package from the debian official repository ‘qtile’

Reference : https://docs.qtile.org/en/latest/manual/install/ 

**Fedora Based **

sudo dnf install mate-polkit rofi i3lock-fancy  materia-gtk-theme  flameshot  network-manager-applet  xfce4-power-manager   playerctl  brightnessctl 

sudo dnf install qtile  volumeicon gnome-keyring  xfce4-notifyd  xsel xclip nautilus  ranger   nitrogen feh picom flameshot 

**Arch Linux Based** 

sudo pacman -S rofi i3lock-fancy xclip ttf-roboto gnome-polkit materia-gtk-theme lxappearance flameshot  network-manager-applet  ranger  nautilus xfce4-notifyd xfce4-power-manager playerctl brightnessctl ttf-font-awesome 


**NOTE :	INSTALL DRAGON CURSORS(Opitional) **

Program List 

    • QtileWM  as the window manager – universal : qtile 
    • Roboto  as the fonts – Debian: fonts-roboto   Arch: ttf-roboto (Optional)
    • Anonymous as the fonts – Debian : ttf-anonymous-pro (Optional)
    • Hack  as the fonts – Debian : fonts-hack-ttf Arch:ttf-hack
    • FontAwesome as the fonts – Arch : ttf-font-awesome  Fedora : fontawesome-fonts
    • Sxhkd the program that handles keybinding – universal : sxhkd 
    • Calcurse as the calender  - Universal : calcurse 
    • Rofi as the launcher – universal : rofi 
    • xclip  for  copying screenshots to clipboard package: xclip
    • mate-polkit use for elevating programs that needs root access 
    • Gnome-keyring  is integrated with the user's login, so that their secret storage can be unlocked when the user logins into their session. 
    • (Optional)Materia as GTK theme - Arch Install: materia-gtk-theme debian: materia-gtk-theme Fedora: materia-gtk-theme
    • (Optional)Papirus Dark as the icon theme Universal Install: 
      wget -qO- https://git.io/papirus-icon-theme-install | sh 
    • lxappearance to setup the gtk and icon theme 
    • Flameshot to take screenshot – Universal Install: flameshot
    • network-manager-applet nm-applet is a Network Manager Tray display from GNOME.
    •  xfce4-power-manager XFCE4's power manager is excellent and a great way of dealing with sleep, monitor timeout, and other power management features
    • Playerctl is an application that is use to control media keys 
    • Brightnessctl   This program allows you read and control device brightness. Devices, by default, include backlight and LEDs (searched for in corresponding classes). If omitted, the first found device is selected.
    • Xfce4-notifyd is an application use to display notification. 


**2) Set the themes**

Start lxappearance to active the icon theme and GTK theme Note: for cursor theme , edit ~/.icons/ and /usr/share/icons, for the change to  show up in system wide .   If using  a window manager instead of a desktop environment and you have no icons for folders and files, specify a GTK icon theme. 
If you have e.g. oxygen-icons installed, edit ~/.gtkrc-2.0 or /etc/gtk-2.0/gtkrc and add the following line:  
gtk-icon-theme-name = "oxygen" 
3) Same theme for Qt/KDE applications and GTK applications, and fix missing indicators

First install qt5-style-plugins (debian) qt5-styleplugins(arch) and add this to the bottom of your /etc/environment
XDG_CURRENT_DESKTOP=Unity
QT_QPA_PLATFORMTHEME=gtk2

**4) Read the documentation** 
Reference :        

	-  https://docs.qtile.org/en/latest/manual/config/index.html  
	-  https://docs.qtile.org/en/latest/manual/ref/index.html 
	-  https://docs.qtile.org/en/latest/manual/faq.html 
	-  https://wiki.archlinux.org/index.php/Qtile 
	-  http://www.qtile.org/
	-  https://github.com/FortAwesome/Font-Awesome


**List of Widgets**				 
    -  TextBox 
    -  GroupBox
    -  Sep
    -  CurrentLayout
    -  WindowName
    -  Memory
    -  DF
    -  Clock
    -  Systray
    -  Image 

**List of Layouts** 
-       MonadTall 
-       MonadWide 
-       Matrix
-       Bsp
-       Floating
-  	TreeTab
-       VerticalTile
-  	RatioTile
-  	Max 
