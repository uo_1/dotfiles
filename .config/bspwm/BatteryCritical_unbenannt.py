#!/usr/bin/env python3
import os 
import locale 
import psutil , time 
from datetime import date
'''
This script requries the psutil module in python.  
For more information about this module check : https://pypi.org/project/psutil/ 

Installation 

	pip install psutil 
	
Notice 

Things I wished to implement 

	- Make a beep sound whenever the computer is below 15 % 
	- Display a message when the battery is charging or not charging 
	- Using either cron job to automate this task or any other background service 
	- Enable the service on boot 
	URL with useful information for the above  
	https://stackoverflow.com/questions/16573051/sound-alarm-when-code-finishes 
	https://towardsdatascience.com/how-to-schedule-python-scripts-with-cron-the-only-guide-youll-ever-need-deea2df63b4e
'''

def main(args):
    return 0



if __name__ == '__main__':
    import sys
	
today = date.today()

while(True):    
	# returns a tuple
	battery = psutil.sensors_battery()
	data = divmod(battery.percent,1000000000)
	print("Current Locale of your System is ",locale.setlocale(locale.LC_ALL, ''))
	
	t = time.localtime()
	current_time = time.strftime("%H:%M:%S", t)
	print("Current time of your system is ",current_time)
	
	# Textual month, day and year	
	d2 = today.strftime("%B %d, %Y")
	print("Current date of your system is ", d2)


	print("Battery Display\n")
	print("Power plugged in : ", battery.power_plugged)
    
    
	if battery.percent < 15:
		print("Battery is at critical level\n")
		print("Battery percentage : ",battery.percent)
	elif battery.percent < 20:    
		print("Please Charge your battery\n")
		print("Battery percentage : ",battery.percent)
	elif battery.percent == 100:
		print("Your Battery is Full Charged",end=" ")
	elif battery.power_plugged == True:
		print("Battery is Charging\n")
	elif battery.power_plugged == False:
		print("The Battery is Discharging")
	else:
		print("Nothing to do\n")
	break
	time.sleep(60*60)
	continue 
	
sys.exit(main(sys.argv))
