#!/usr/bin/env bash

# More info : https://github.com/jaagr/polybar/wiki

#Install the following applications for polybar and icons in polybar

# Tip : There are other interesting fonts that provide icons like nerd-fonts-complete


# Terminate already running bar instances
killall -q polybar 

# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done


desktop=$(echo $DESKTOP_SESSION)
count=$(xrandr --query | grep " connected" | cut -d" " -f1 | wc -l)


case $desktop in

    bspwm|/usr/share/xsessions/bspwm)
    if type "xrandr" > /dev/null; then
      for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
        MONITOR=$m polybar --reload mainbar-bspwm -c ~/.config/polybar/config &
      done
    else
    polybar --reload mainbar-bspwm -c ~/.config/polybar/config &
    fi
    # second polybar at bottom
    # if type "xrandr" > /dev/null; then
    #   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    #     MONITOR=$m polybar --reload mainbar-bspwm-extra -c ~/.config/polybar/config &
    #   done
    # else
    # polybar --reload mainbar-bspwm-extra -c ~/.config/polybar/config &
    # fi
    ;;

esac


# Launch Polybar, using default config location ~/.config/polybar/config
# polybar example &

# polybar top -c ~/.config/polybar/config-top.ini & 
# polybar bottom -c ~/.config/polybar/config-bottom.ini & 





#echo "Polybars launched..."

