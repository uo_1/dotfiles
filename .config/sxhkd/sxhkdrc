#Sxhkdrc File 
# KeyBind Set 

# Command Keys                  Description         
# bspc node -t tiled            = switch the window to tiled state   
# bspc node -t floating  	= switch the window to floating state 
# bspc node -t pseudo_tiled	= switch the window to pseudo_tiled state 
# bspc node -t fullscreen 	= switch the window to fullscreen state        
# bspc node -c 			= close a window 
# bspc node -k			= kill a window 
# bspc node -d7 			= send the window to 7 desktop
# bspc node -p north		= east
# bspc node -f next.local.!hidden.window.floating  = allow to switch between different floating windows 


# terminal emulator
 super + t
	urxvt

# terminal emulator (Terminator)
ctrl + alt + t 
	terminator

# Picom Toggle Script 
ctrl + alt + o
	~/scripts/picom-toggle.sh

# Poweroff
super + Delete
	~/.config/polybar/scripts/powermenu.sh

# show help on keybinds (Requires rxvt-unicode xsel) 
super + Escape 
	urxvt -geometry 170x30 -name urxvt_keys_help -e ~/scripts/Keybinds.sh 

# Display Keybinds (Requires rxvt-unicode xsel)
alt + Escape 	
	 urxvt -geometry 130x30 -name urxvt_keys_help -e ~/scripts/Keybinds1.sh 

# program launcher
super + r
   rofi -combi-modi window,drun,ssh -themes solarized -font "hack 10" -show combi -icon-theme "Papirus" -show-icons
       #rofi  -combi-modi window,drun,ssh  -font "hack 10"  

#Launch Filemanager (Nautilus)
super + shift + f 
  nautilus

# Launch TextEditor (Gedit)
super + shift + t 
  gedit  


# Launch TextEditor (Geany)
super +  e 
     geany 

# Launch MusicPlayer (Audacious)
super + shift + m 
	audacious

# Launch VideoPlayer (Smplayer)
super + shift + v
	smplayer

# Launch Browser (Brave-Browser)
super + shift + b 
      brave-browser 

# Launch Browser (Falkon)
super + b 
   falkon 


# Launch Browser (Chromium)
ctrl + alt + b
	chromium -no-default-browser-check

# make sxhkd reload its configuration files:
super + @space
	pkill -USR1 -x sxhkd

# Flip Layout Vertically/Horizontally 
alt + super {v,i}
     bspc node @/ --flip {vertical,horizontal}

# Change Layouts 
alt + control + {1,2,3}
    ~/.config/bspwm/config_scheme.sh {first_child,longest_side,spiral}   


# Launch Screenshot Utility (Flameshot)
Print 
  flameshot full -p ~/Pictures

# Launch Screenshot Utility (Flameshot)
super + Print 
      flameshot gui 

# Launch System Manager Tool (Xfce4-Power-Manager-Settings) 
alt + Insert
	xfce4-power-manager-settings

# Launch Virtual Machine (Virt-manager)
alt + Return 
      virt-manager  

# Launch Virtual Machine ( VirtualBox )
super + v 
	VirtualBox Manager	

# Launch Virtual Machine ( Vmware Player )
alt  + v 
	 vmplayer  	


# Launch Nitrogen 
super + n 
  nitrogen

# Lockscreen 
super + x 
#    i3lock-fancy
    light-locker-command -l   	


# quit/restart bspwm
super + alt + {q,r}
	bspc {quit,wm -r}

# quit/restart bspwm 
ctrl + super + {q,r}
    bspc {quit,wm -r}

# close and kill
alt + {c,w}
	bspc node -{c,k}

# alternate between the tiled and monocle layout
alt + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest window
super + g
	bspc node -s biggest.window

#
# state/flags
#

# set the window state
alt + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}


# set the node flags
alt + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

# Desktop[Focus/Swap] 
 

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous window in the current desktop
super + c
	bspc node -f {next,prev}.local.!hidden.window

# Allow to switch between floating windows 
super + bracket{left,right}
	bspc node -f  next.local.!hidden.window.floating  

# focus the next/previous desktop 
super + {w,s}
        bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'


# swap the current node and the biggest node 
alt + g 
      bspc node -s biggest.local


# preselect


# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + Return
     bspc node -p cancel

# cancel the preselection for the focused desktop
super + shift + Return 
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super +  shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0} 

# allow to switch between different floating windows 
super + bracket{left,right}
	 bspc node -f next.local.!hidden.window.floating


# Hide/Unhide Window
super + shift + i
	~/.config/bspwm/winmask	


# Volume Control 
# Using Playerctl can be use to control media keys
XF86Audio{Play,Prev,Next,Stop}
	playerctl {play-pause,previous,next,stop,toggle}


# Using ALSA as it is present by default in most cases  
XF86Audio{Raise,Lower}Volume
	amixer sset Master {5%+,5%-}
XF86AudioMute
	amixer sset Master toggle
XF86AudioMicMute
	amixer sset Master micmute
#	pactl set-source-mute 1 toggle
#xblacklight Use to Controled Brightness
#XF86MonBrightness{Up,Down}
#	xbacklight {-inc 5,-dec 5}

# Control Brightness (brightnessctl is required)
XF86MonBrightness{Up,Down} 
 	brightnessctl s 10{+,-} 	
