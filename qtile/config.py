# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Drag, Key, Screen, Group, Match, Drag, Click, Rule 
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook 
from libqtile.widget import Spacer  
from libqtile.images import Img 

#mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
browser = "brave-browser"
term = 'urxvt'			# This is my default terminal 
home = os.path.expanduser('~')


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [

# FUNCTION KEYS

    Key([mod], "z",lazy.spawn(term + ' --drop-down')),

# SUPER + FUNCTION KEYS

    Key([mod], "e", lazy.spawn('gedit'), desc="launch text editor"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),
    Key([mod], "m", lazy.spawn('smplayer'), desc="launch music player"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "r", lazy.spawn('rofi -combi-modi window,drun,ssh-themes solarized -font "hack 10" -show combi -icon-theme "Papirus" -show-icons')),
    Key([mod], "t", lazy.spawn('urxvt'), desc="launch terminal"),
    Key([mod], "s", lazy.spawn('pavucontrol')),
    Key([mod], "w", lazy.spawn('falkon')),
    Key([mod], "x", lazy.spawn('i3lock-fancy')),   
    Key([mod], "b", lazy.spawn('brave-browser')),      
    Key([mod], "Escape", lazy.spawn('xterm')),
    Key([mod], "Return", lazy.spawn('virt-manager')), 
    Key([mod], "Delete", lazy.spawn(home + '/scripts/powermenu')),
    Key([mod], "p", lazy.spawn('psensor')),
    Key([mod], "g", lazy.spawn('gimp')),
    Key([mod], "v", lazy.spawn('virtualbox')),

# SUPER + SHIFT KEYS

   Key([mod, "shift"], "d", lazy.spawn("dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'")),
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod, "shift"], "b", lazy.spawn('vivaldi-stable')),      
    Key([mod, "shift"], "m", lazy.spawn('audacious')), 
    Key([mod, "shift"], "t", lazy.spawn('gedit')),    
              
# CONTROL + ALT KEYS

#    Key(["mod1", "control"], "b", lazy.spawn('chromium -no-default-browser-check')),
#    Key(["mod1", "control"], "m", lazy.spawn('xfce4-settings-manager')),
#    Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
#    Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
#    Key(["mod1", "control"], "r", lazy.spawn('rofi-theme-selector')),
#    Key(["mod1", "control"], "t", lazy.spawn('terminator')),

# ALT + ... KEYS

#    Key(["mod1"], "f", lazy.spawn('nautilus')),
#    Key(["mod1"], "h", lazy.spawn(term + ' -e htop')),
#    Key(["mod1"], "u", lazy.spawn(term +  ' -e sudo pacman -Syu')),
#    Key(["mod1"], "n", lazy.spawn('nitrogen')),
#    Key(["mod1"], "Return", lazy.spawn(home + '/.config/qtile/scripts/powermenu')),
    Key(["mod1"], "c", lazy.window.kill()),

# SCREENSHOTS    

    Key([mod], "Print", lazy.spawn('flameshot gui')),

# MULTIMEDIA KEYS

# INCREASE/DECREASE BRIGHTNESS
#    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s 10+")),
#    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 10-")),

# INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),
    Key([], "XF86AudioMicMute",lazy.spawn("pactl set-source-mute 1 toggle")),

# QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# SPLIT LAYOUT FOR BSP
    Key([mod, "mod1"], "s", lazy.layout.toggle_split()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
]   


groups = []

	
# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]

# Other ICONS = ["","","","","","","","","","","","","",""]

group_labels = ["", "", "", "", "", "", "", "", "", "",]

#group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = ["monadtall", "monadwide", "matrix", "bsp", "floating", "treetab", "verticaltile", "bsp", "max","monatall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
       Key([mod], "Tab", lazy.screen.next_group()),
       Key(["mod1"], "Tab", lazy.screen.prev_group()), 
   	Key([mod], "a", lazy.screen.toggle_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
#        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])


def init_layout_theme():
    return {"margin":5,
            "border_width":2,
           #"border_focus": "#5e81ac",
	    "border_focus": "#9fef00",
	    "border_normal": "#062d2a",	
           #"border_normal": "#4c566a"
            }

layout_theme = init_layout_theme()


layouts = [ 
    layout.MonadTall(margin=8, border_width=2, border_focus="#9fef00", border_normal="#062d2a"),
    layout.MonadWide(margin=8, border_width=2, border_focus="#9fef00", border_normal="#062d2a"),
    #layout.MonadTall(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    #layout.MonadWide(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme), 
    layout.TreeTab(
	font = "Hack Regular", 
	fontsize = 12,
	sections = ["FIRST", "SECOND"],      
        section_fontsize = 11,
	bg_color = "141414",
	active_bg = "9fef00",
	active_fg = "111010",
	inactive_bg = "2b3808",
        inactive_fg = "a0a0a0", 
	vspace = 2,
	padding_y = 5,         
        section_top = 10,        
	panel_width = 320
	),
    layout.VerticalTile(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]

# COLORS FOR THE BAR

def init_colors():
    return [ 
	    ["#2F343F", "#2F343F"], # color 0
	    ["#062d2a", "#062d2a"], # color 1 
           #["#2F343F", "#2F343F"], # color 1
            ["#c0c5ce", "#c0c5ce"], # color 2
            ["#fba922", "#fba922"], # color 3
            ["#3384d0", "#3384d0"], # color 4
            ["#f3f4f5", "#f3f4f5"], # color 5
            ["#cd1f3f", "#cd1f3f"], # color 6
            ["#62FF00", "#62FF00"], # color 7
            ["#6790eb", "#6790eb"], # color 8
            ["#a9a9a9", "#a9a9a9"]] # color 9


colors = init_colors()


# WIDGETS FOR THE BAR

def init_widgets_defaults():
    return dict(font="Noto Sans",
                fontsize = 12,
                padding = 2,
                background=colors[1])

widget_defaults = init_widgets_defaults()

def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
	       widget.TextBox(	
			font="FontAwesome",
			text = "  ",
			padding = 3,
			fontsize = 16,
			foreground = colors[3],
			background = colors[1],
		        mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn('rofi -combi-modi window,drun,ssh -themes solarized -font "hack 10" -show combi -icon-theme "Papirus" -show-icons')}
			),
	       widget.GroupBox(
		        font="FontAwesome",
                        fontsize = 16,
                        margin_y = -1,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = colors[9],
                        inactive = colors[5],
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = colors[7],
                        foreground = colors[3],
                        background = colors[1]
                        ),              
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.CurrentLayout(
                        font = "Hack Regular",
			#font = "Noto Sans",
                        foreground = colors[5],
                        background = colors[1]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.WindowName(
               		#font="Noto Sans",
               		font="Hack Bold",
                        fontsize = 12,
                        foreground = colors[5],
                        background = colors[1],
                        ),
#	       widget.CPUGraph(	
#                        border_color = colors[2],
#                        fill_color = colors[7],
#                        graph_color = colors[7],
#                        background=colors[1],
#                        border_width = 1,
#                        line_width = 1,
#                        core = "all",
#                        type = "box"
#                        ),
#	       widget.CPU(
##			format = '{load_percent}% ',
#			format = 'CPU {freq_current}GHz {load_percent}%',
#			font = "Hack Regular",
#			font = "Noto Sans",
#			fontsize = 12,
#			foreground = colors[7],
#			background = colors[1],
#			update_interval = 3
#			),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[7],
                        #foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[7],
                        background=colors[1],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Memory(
			#font="Noto Sans",   
                        font="Hack Regular",
                        format = '{MemUsed}M/{MemTotal}M',
                        update_interval = 1,
                        fontsize = 12,
                        foreground = colors[5],
                        background = colors[1],
                       ),
               widget.DF(
                        font="Hack Regular",
                       #font="Noto Sans",
                        format = '{p} ({uf}{m}|{r:.0f}%)',
                        partition = '/',
			padding = 0,
			measure = 'G',
                        update_interval = 60,
                        warn_space = 10,
	      	        warn_color = colors[3],
	      	        visible_on_warn = 'True', 		       
                        fontsize = 12,
                        foreground = colors[5],
                        background = colors[1],
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[7],
                        #foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[7],
                        background=colors[1], 	
			mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(term + ' -e calcurse')},
                        padding = 0,
			fontsize=16
	               ), 
               widget.Clock(
                        foreground = colors[5],
                        background = colors[1],
                        #font="Noto Sans",
                        font = "Hack Regular",
                        fontsize = 12,
                        format="%b %Y-%m-%d %a %T"
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[7],
                        #foreground = colors[2],
                        background = colors[1]
                        ),
	       widget.TextBox(
			font ="FontAwesome",
			text ="",
			padding = 0,
			mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('rofi -show window')},
			foreground=colors[3],
			background=colors[1]
			),
#	       widget.CurrentScreen(
#			font = "Hack Regular",
#			font = "Noto Sans",
#			fontsize = 10,
#			padding = 2,
#			active_color = '00ff00',
#			active_text = 'A',
#			inactive_color = 'ff0000',
#			inactive_text = 'I',
#			foreground = colors[2],
#			background = colors[1]
#			),
	       widget.Systray(
                        background=colors[1],
                        icon_size=20,
                        padding = 4
                        ),
	       widget.Image(
			filename = '~/.config/qtile/icons/flameshot.png',
			mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('flameshot gui')}
			),
               widget.Image(
			filename = '~/.config/qtile/icons/power.png',
			mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(home + '/.config/qtile/scripts/powermenu')}	
			),
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click(["mod1"], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

@hook.subscribe.client_new
def assign_app_group(client):
     d = {}
#     #########################################################
#     ################ assgin apps to groups ##################
#     #########################################################
#     d["1"] = ["Nightly", "Subl3", "nightly", "subl3" ]
     d["2"] = [ "Chromuim", "chromuim" ]
#     d["3"] = ["Inkscape", "Nitrogen", "Feh",
#               "inkscape", "nitrogen", "feh", ]
#     d["4"] = ["Gimp", "gimp" ]
     d["5"] = [ "Zathura", "org.pwmt.zathura","Viewnior","viewnior" ]
#     d["6"] = ["Vlc","vlc", "Mpv", "mpv" ]
     d["7"] = [ "Virt-Manager", "virt-manager" ]
     d["8"] = ["org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt","Brave-browser",
               "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt","brave-browser" ]
#     d["9"] = ["Mail", "Thunderbird",
#               "mail", "thunderbird" ]
     d["0"] = [  "Audacious", "Smplayer",
                 "audacious", "smplayer" ]
#     ##########################################################
     wm_class = client.window.get_wm_class()[0]

     for i in range(len(d)):
         if wm_class in list(d.values())[i]:
             group = list(d.keys())[i]
             client.togroup(group)
             client.group.cmd_toscreen()

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME


main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
	# Run the utility of 'xprop' to see the wm class and name of an X client.
        # default_float_rules include: utility, notification, toolbar, splash, dialog,
	# file_progress, confirm, download and error.      
	*layout.Floating.default_float_rules,
        Match(wm_class = 'feh'),
        Match(wm_class = 'Arandr'),
        Match(wm_class = 'ssh-askpass'),

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart

wmname = "Qtile"
